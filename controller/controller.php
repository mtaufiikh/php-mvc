<?php

	include "model/model.php";
	
	class Controller{
	
		function __construct(){
			$this->model = new Model(); 
		}
		
		function index(){
			$data = $this->model->selectAll(); 
			include "view/view.php"; 
		}
		
		function viewEdit($nim){
			$data = $this->model->selectMhs($nim); 
			$row = $this->model->fetch($data); 
			include "view/view_edit.php"; 
		}
		
		function viewInsert(){
			include "view/view_add.php"; 
		}

		function update(){
			$nim = $_POST['nim'];
			$nama = $_POST['nama'];
			$angkatan = $_POST['angkatan'];
			$fakultas = $_POST['fakultas'];
			$prodi = $_POST['prodi'];
			
			$update = $this->model->updateMhs($nim, $nama, $angkatan, $fakultas, $prodi);
			header("location: index.php");
		}
		
		function delete($nim){
			$delete = $this->model->deleteMhs($nim);
			header("location: index.php");
		}
		
		function insert(){
			$nim = $_POST['nim'];
			$nama = $_POST['nama'];
			$angkatan = $_POST['angkatan'];
			$fakultas = $_POST['fakultas'];
			$prodi = $_POST['prodi'];
			
			$insert = $this->model->insertMhs($nim, $nama, $angkatan, $fakultas, $prodi);
			header("location: index.php");
		}
		
		function __destruct(){
		}
	}
?>